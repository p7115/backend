# ProjetDevWeb (Backend)

Repository du backend de l'application.
Ecrit en Java, avec Spring, en utilisant une base de données H2, avec un swagger à disposition (ajouter /swagger.html à l'URL).

## Serveur de développement

Depuis un IDE, exécuter la classe main de l'application `com.tt.back.BackApplication.java`. Accès à l'adresse http://localhost:8081/.

## Build

Exécuter `mvn package`. Build disponible dans le dossier `target/`.

## Docker

- Bien penser à build le projet avant ! (Voir point précédent).
- Exécuter `docker build -t ossacipe/projetdevweb-backend .` pour construire l'image.
- Pour lancer un conteneur avec l'image, exécuter `docker run IMAGE ossacipe/projetdevweb-backend`.
- Accès à l'URL http://localhost:8081/.

## DockerHub

[Accès ici](https://hub.docker.com/r/ossacipe/projetdevweb-backend)
