package com.tt.back.repository;

import com.tt.back.entity.Tva;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TvaRepository extends JpaRepository<Tva, Long> {
}